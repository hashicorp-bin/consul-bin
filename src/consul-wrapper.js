#!/usr/bin/env node
'use strict';

const { spawn } = require('child_process');
const { resolve } = require('path');

const execName = process.platform === 'win32' ? 'consul.exe' : './consul';
const command = resolve(__dirname, '..', 'tools', execName);
const consul = spawn(command, process.argv.slice(2), { cwd: process.cwd() });

consul.stdout.pipe(process.stdout);
consul.stderr.pipe(process.stderr);
consul.on('error', function(err) {
  console.error(`Received an error while executing the Consul binary: ${err}`);
  process.exit(1);
});
